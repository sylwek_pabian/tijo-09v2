package com.company;

public class Bank implements BankOperation {
    @Override
    public int createAccount() {
        return 1;
    }
    @Override
    public int deleteAccount(int accountNumber) {
        return ACCOUNT_NOT_EXISTS;
    }
    @Override
    public boolean deposit(int accountNumber, int amount) {
        return false;
    }
    @Override
    public boolean withdraw(int accountNumber, int amount) {
        return false;
    }
    @Override
    public boolean transfer(int fromAccount, int toAccount, int amount) {
        return false;
    }
    @Override
    public int accountBalance(int accountNumber) {
        return ACCOUNT_NOT_EXISTS;
    }
    @Override
    public int sumAccountsBalance() {
        return 0;
    }
}
